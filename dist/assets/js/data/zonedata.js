var zones = [
  {
    name: "Arach",
    coords: [-60.33,-141.15]
  },
  {
    name: "Arm of Okran",
    coords: [65.98,-25.75]
  },
  {
    name: "Ashlands",
    coords: [-78.38,120.59]
  },
  {
    name: "Bast",
    coords: [73.75,14.15]
  },
  {
    name: "Bonefields",
    coords: [-69.41,8.26]
  },
  {
    name: "Border Zone",
    coords: [-6.66,-55.20]
  },
  {
    name: "Burning Forest",
    coords: [-51.18,-17.58]
  },
  {
    name: "Cheaters Run",
    coords: [-81.95,47.99]
  },
  {
    name: "Darkfinger",
    coords: [82.24,-9.76] // #wrong coords
  },
  {
    name: "Deadlands",
    coords: [14.26,8.96]
  },
  {
    name: "Dreg",
    coords: [37.30,-118.04]
  },
  {
    name: "Fishman Island",
    coords: [-81.36,-9.32]
  },
  {
    name: "Flats Lagoon",
    coords: [-50.23,22.06]
  },
  {
    name: "Floodlands",
    coords: [69.16,-71.67]
  },
  {
    name: "Fog Islands",
    coords: [41.24,-96.68]
  },
  {
    name: "Forbidden Isle",
    coords: [21.37,164.22]
  },
  {
    name: "Greenbeach",
    coords: [-34.23,158.03]
  },
  {
    name: "Grey Desert",
    coords: [13.24,32.17]
  },
  {
    name: "Greyshelf",
    coords: [-69.44,53.26]
  },
  {
    name: "Gut",
    coords: [23.08,79.72]
  },
  {
    name: "Heng",
    coords: [52.21,77.43]
  },
  {
    name: "High Bonefields",
    coords: [-67.36,-19.20]
  },
  {
    name: "Howler Maze",
    coords: [40.65,107.40]
  },
  {
    name: "Iron Valleys",
    coords: [40.45,-0.18]
  },
  {
    name: "Obedience",
    coords: [57.30,-89.30]
  },
  {
    name: "Okran 's Gulf",
    coords: [39.23,-68.03]
  },
  {
    name: "Okran 's Pride",
    coords: [39.54,-42.10]
  },
  {
    name: "Okran 's Valley",
    coords: [68.27,-22.15]
  },
  {
    name: "Raptor Island",
    coords: [61.10,-142.03]
  },
  {
    name: "Rebirth",
    coords: [57.66,-55.99]
  },
  {
    name: "Royal Valley",
    coords: [-77.27,42.89]
  },
  {
    name: "Shem",
    coords:  [-24.63,-4.28]
  },
  {
    name: "Shun",
    coords: [-71.80,-140.10]
  },
  {
    name: "Sinkuun",
    coords: [81.23,25.49]
  },
  {
    name: "Skimsands",
    coords: [54.24,13.18]
  },
  {
    name: "Skinner 's Roam",
    coords: [17.06,-16.79]
  },
  {
    name: "Sniper Valley",
    coords: [-68.27,88.77]
  },
  {
    name: "Sonorous Dark",
    coords: [-75.67,92.29]
  },
  {
    name: "South Wetlands",
    coords: [-69.41,-58.54]
  },
  {
    name: "Spider Plains",
    coords: [-55.28,-107.75]
  },
  {
    name: "Spine Canyon",
    coords: [59.31,21.80]
  },
  {
    name: "Stenn Desert",
    coords: [-28.92,-105.64]
  },
  {
    name: "Stobe 's Gamble",
    coords: [-51.51,70.93]
  },
  {
    name: "Stobe 's Garden",
    coords: [-39.30,124.28]
  },
  {
    name: "Stormgap Coast",
    coords: [9.71,96.50]
  },
  {
    name: "The Black Desert",
    coords: [32.92,25.22]
  },
  {
    name: "The Crags",
    coords: [-56.66,106.52]
  },
  {
    name: "The Crater",
    coords: [-71.02,-100.37]
  },
  {
    name: "The Great Desert",
    coords: [73.33,63.28]
  },
  {
    name: "The Hook",
    coords: [-77.20,-84.90]
  },
  {
    name: "The Outlands",
    coords:[-9.97,112.76]
  },
  {
    name: "The Pits",
    coords: [-63.23,122.87]
  },
  {
    name: "The Pits East",
    coords: [-61.19,154.69]
  },
  {
    name: "The Swamp",
    coords: [-47.40,-50.80]
  },
  {
    name: "The Unwanted Zone",
    coords: [-23.08,92.55]
  },
  {
    name: "Vain",
    coords: [-2.28,-118.13]
  },
  {
    name: "Venge",
    coords: [-17.81,45.44]
  },
  {
    name: "Watcher 's Rim",
    coords: [-58.54,-133.77]
  },
  {
    name: "Wend",
    coords: [59.22,-42.63]
  }
];